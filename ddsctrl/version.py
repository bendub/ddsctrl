""" ddsctrl/version.py """
__version__ = "0.5.1"

# 0.5.1   (27/06/2019): Update dds driver name package (dds -> ad9xdds).
# 0.5.0   (19/06/2019): Move to PyQt5 library.
